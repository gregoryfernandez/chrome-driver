import requests
import json
import os
import re
import zipfile
import sys
from win32com.client import Dispatch
from packaging import version
from packaging.version import Version

chrome_executable64 = 'C:/Program Files/Google/Chrome/Application/chrome.exe'
chrome_executable32 = 'C:/Program Files (x86)/Google/Chrome/Application/chrome.exe'
chrome_for_testing_executable = 'C:/Repositories/Tools/chrome-win64/chrome.exe'
chrome_versions = 'https://googlechromelabs.github.io/chrome-for-testing/last-known-good-versions-with-downloads.json'

edge_executable = 'C:/Program Files (x86)/Microsoft/Edge/Application/msedge.exe'
firefox_versions = 'https://api.github.com/repos/mozilla/geckodriver/releases'

platform = 'win64'
location = 'C:/Program Files/webdriver'
# location = 'C:/Repositories/Tools'
chrome_for_testing = True

cleanup_files = [
    f'chromedriver-{platform}.zip',
    f'chrome-{platform}.zip',
    f'edgedriver_{platform}.zip',
    f'geckodriver-win32.zip',
]

def get_exe_version(file_path: str):
    parser = Dispatch('Scripting.FileSystemObject')
    try:
        version = parser.GetFileVersion(file_path)
    except Exception:
        print(f'Could not find {file_path}')
        return '0.0'
    return version

def get_driver_version(driver_name: str):
    # Run [driver_name] --version and return the output
    driver_version_all = os.popen(f'{driver_name} --version').read()
    # Search for spaces in the output.
    version_whitespace = [m.start() for m in re.finditer(' ', driver_version_all)]
    # Driver will be in between the first two spaces.
    try:
        if driver_name == 'msedgedriver':
            return driver_version_all[version_whitespace[2] + 1:version_whitespace[3]]
        else:
            return driver_version_all[version_whitespace[0] + 1:version_whitespace[1]]
    except IndexError:
        print(f'Could not find {driver_name}')
        return '0.0'

def do_chrome(file_location: str):
    print('Checking Chrome:')
    # Get current browser version
    browser_version = get_exe_version(chrome_executable64)

    # try 32 bit install path
    if browser_version == '0.0':
        browser_version = get_exe_version(chrome_executable32)

    print(f'Chrome Browser version: {browser_version}')

    if browser_version == '0.0':
        print(f'Chrome installation not found at: {chrome_executable64} or {chrome_executable32}')
        return

    # Get current driver version
    driver_version = get_driver_version('chromedriver')
    print(f'Chrome Driver verison: {driver_version}')

    # Get latest stable version
    response = json.loads(requests.get(chrome_versions).text)
    latest_stable = response['channels']['Stable']['version']

    if Version(browser_version).release[0] > Version(driver_version).release[0]:
        # Get latest stable below browser version if there is no exact match

        if version.parse(latest_stable) <= version.parse(browser_version):
            driver_url = f'https://edgedl.me.gvt1.com/edgedl/chrome/chrome-for-testing/{latest_stable}/{platform}/chromedriver-{platform}.zip'

            print(f'Downloading chrome driver version {driver_version} for browser version {browser_version}')

            driver_binary = requests.get(driver_url)
            driver_file = f'{file_location}/chromedriver-{platform}.zip'

            # Write chromedriver zip file
            with open(driver_file, "wb") as code:
                code.write(driver_binary.content)

            # Extract chromedriver
            with zipfile.ZipFile(driver_file, 'r') as zip_ref:
                with open(f'{file_location}/chromedriver.exe', 'wb') as file:
                    file.write(zip_ref.read(f'chromedriver-{platform}/chromedriver.exe'))
    else:
        print(f'chromedriver ({driver_version}) is compatible with browser version ({browser_version})')

def do_edge(file_location: str):
    print('Checking Edge:')
    browser_version = get_exe_version(edge_executable)
    print(f'Edge Browser version: {browser_version}')

    if browser_version == '0.0':
        print(f'Edge browser not found at: {edge_executable}')
        return

    # Get current driver version
    driver_version = get_driver_version('msedgedriver')
    print(f'Edge Driver verison: {driver_version}')

    if browser_version != driver_version:
        print(f'Downloading edge driver version {driver_version} for browser version {browser_version}')
        driver_url = f'https://msedgedriver.azureedge.net/{browser_version}/edgedriver_{platform}.zip'
        driver_binary = requests.get(driver_url)
        driver_file = f'{file_location}/edgedriver_{platform}.zip'

        with open(driver_file, "wb") as code:
            code.write(driver_binary.content)

        # Extract chromedriver
        with zipfile.ZipFile(driver_file, 'r') as zip_ref:
            with open(f'{file_location}/msedgedriver.exe', 'wb') as file:
                file.write(zip_ref.read(f'msedgedriver.exe'))
    else:
        print(f'msedgedriver ({driver_version}) is the same as browser version ({browser_version})')


def do_firefox(file_location: str):
    """
    For firefox, geckodriver supports ~30 older versions of the browser, so there is no harm in keep this up to date
    with the latest version.

    Additionally, gecko only releases in 32-bit, so platform will be irrelevant here.
    """
    print('Checking Firefox:')
    # Get current driver version
    driver_version = get_driver_version('geckodriver')
    print(f'FF Driver verison: {driver_version}')

    response = json.loads(requests.get('https://api.github.com/repos/mozilla/geckodriver/releases').text)
    latest_stable = response[0]['tag_name']
    print(f'Latest Stable version: {latest_stable}')

    if version.parse(latest_stable) > version.parse(driver_version):
        print(f'Downloading driver version: {latest_stable}')
        driver_url = f'https://github.com/mozilla/geckodriver/releases/download/{latest_stable}/geckodriver-{latest_stable}-win32.zip'
        driver_binary = requests.get(driver_url)
        driver_file = f'{file_location}/geckodriver-win32.zip'

        with open(driver_file, "wb") as code:
            code.write(driver_binary.content)

        # Extract chromedriver
        with zipfile.ZipFile(driver_file, 'r') as zip_ref:
            with open(f'{file_location}/geckodriver.exe', 'wb') as file:
                file.write(zip_ref.read(f'geckodriver.exe'))
        print(f'Downloaded diver version {latest_stable}')
    else:
        print(f'Local geckodriver version ({driver_version}) is the same as latest stable version ({latest_stable})')


def cleanup(file_location):
    """
    Clean up zip files
    """
    for file in os.listdir(file_location):
        if file in cleanup_files:
            print(f'Cleanup {file}')
            os.remove(f'{file_location}/{file}')


if __name__ == '__main__':
    args = sys.argv
    # arg[0] is the script name
    if len(args) == 2:
        location = args[1]

    do_chrome(location)
    do_edge(location)
    do_firefox(location)
    cleanup(location)
