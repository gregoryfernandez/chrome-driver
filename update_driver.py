import requests
import os
import platform
import zipfile
from win32com.client import Dispatch


os_ver = 'win32'
url = 'https://chromedriver.storage.googleapis.com/LATEST_RELEASE_'
url_file = 'https://chromedriver.storage.googleapis.com/'
file_name = 'chromedriver_' + os_ver + '.zip'
version = '97.0.4692'


def get_version_via_com(filename):
    parser = Dispatch("Scripting.FileSystemObject")
    try:
        version = parser.GetFileVersion(filename)
    except Exception:
        return None
    return version


# Set OS Version
if platform.system() == 'Windows':
    os_ver = 'win32'

# Get Chrome Version
paths = [r"C:\Program Files\Google\Chrome\Application\chrome.exe",
         r"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe"]
chrome_version = list(filter(None, [get_version_via_com(p) for p in paths]))[0]
print(chrome_version)

# Set Chromedriver version
version = chrome_version[0:9]
print(version)

# Get chromedriver.exe location
driver_dir = os.popen('where chromedriver.exe').read()
driver_dir = driver_dir.rstrip(driver_dir[-1]).rstrip('chromedriver.exe')
print(driver_dir)

# Download chromedriver
version_response = requests.get(url + version)

if version_response.text:
    file = requests.get(url_file + version_response.text + '/' + file_name)
    with open(file_name, "wb") as code:
        code.write(file.content)

# Unzip Chromedriver to driver_dir
with zipfile.ZipFile(file_name, 'r') as zip_ref:
    zip_ref.extractall(driver_dir)